import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;

public class TestWebSite extends MainMethods{
    static WebDriver driver;
    static String baseURL;

    @Test
    public void instructions() throws IOException, InterruptedException {

      MainMethods mm = new MainMethods();
      mm.setup();
      mm.checkauthor();
      mm.checkimage();
      mm.checkauthorandimagesize();
      mm.checknewsandbrowserheader();
      mm.closeChrome();

    }

}

