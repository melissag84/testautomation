import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

public class MainMethods {
    public static WebDriver driver;
    static String baseURL;

    public void setup() throws IOException, InterruptedException {
        baseURL = "https://techcrunch.com/";
        System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
        driver=new ChromeDriver();
        driver.get(baseURL);
        driver.manage().window().maximize();
        Thread.sleep(1000);
        String A = driver.getTitle();
        if (A.equals("TechCrunch – Startup and Technology News"))
            System.out.println("Page Title matches");
        else
            System.out.println("Page Title Doesn't Match");
    }

    public void checkauthor() throws IOException {
        try {
            List<WebElement> authors = driver.findElements(By.xpath("//span[@class='river-byline__authors']/span"));
            System.out.println(authors.size());
            for (WebElement webElement : authors) {
              Boolean state = authors.isEmpty();
                System.out.println(state);

              if (state==true)
                System.out.println("Author name is empty");
               else
                System.out.println("Author name is not empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{

        }
    }
    public void checkimage() throws IOException {
        try {
            List<WebElement> images = driver.findElements(By.xpath("//figure[@class='post-block__media']/picture"));
            System.out.println(images.size());
            for (WebElement webElement : images) {
                Boolean state = images.isEmpty();
                System.out.println(state);

                if (state==true)
                    System.out.println("Image is empty");
                else
                    System.out.println("Image is not empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{

        }
    }
    public void checkauthorandimagesize() throws IOException {
        try {
            List<WebElement> images = driver.findElements(By.xpath("//figure[@class='post-block__media']/picture"));
            int imagessize=images.size();
            List<WebElement> authors = driver.findElements(By.xpath("//span[@class='river-byline__authors']/span"));
            int authorssize= authors.size();
            if (imagessize==authorssize)
                    System.out.println("Every Author has the image");
                else
                    System.out.println("Every Author has not the image." + " "+"Author size is " + authorssize + "Image size is " + imagessize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{

        }
    }
    public void checknewsandbrowserheader() throws IOException {
        try {
            driver.findElement(By.xpath("//*[@id=\"tc-main-content\"]/div[3]/div/div/div/article[1]/header/h2/a")).click();
            Thread.sleep(1000);
            List<WebElement> linkurls = driver.findElements(By.xpath("//div[@class='article-content']/p//a[@href]"));
            int urlsize=linkurls.size();
            System.out.println("There are "+ urlsize + "links in new content");
            String header = driver.findElement(By.xpath("//header[@class='article__header']//h1[@class='article__title']")).getText();
            System.out.println(header);
            String browserheader = driver.getTitle();
            System.out.println(browserheader);
            if(browserheader.contains(header))
                System.out.println("Browserheader and new header are same");
            else
            System.out.println("Browserheader and new header are not same");


        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{

        }
    }

    public void closeChrome(){
        driver.quit();
    }
        }









